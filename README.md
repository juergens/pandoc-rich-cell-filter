# pandoc rich cell filter

adds special syntax to make it easier to create tables with rich content with pandoc in markdown

## status

done: 

- idea
- basic syntax

todo: 

- requirements
- implementation


## syntax

### minimal example
    
    ~~~rich-cell-table
    h1
    ...
    h2
    ...
    h3
    :::
    a1
    ...
    a2
    ...
    a3
    :::
    b1
    ...
    b2
    ...
    b3
    ~~~

becomes: 

h1|h2|h3
--|--|--
a1|a2|a3
b1|b2|b3


### rich example 

    ~~~rich-cell-table
    
    ### Name
    
    ...
    
    ### content
    
    :::
    
    #### sub-table
    
    ...
    
    h1|h2|h3
    --|--|--
    a1|a2|a3
    b1|b2|b3

    :::hspan 1
    
    #### wide cell
    
    :::vspan 2
    
    #### first cell long
    
    ... 
    
    cell next to vspanned cell on same row
    
    :::
    
    cell next to vspanned cell on next row
    
    ~~~
    
becomes 

<table>
<th>

### Name

</th>
<th>

### content
    
</th>
<tr>
<td>

#### sub-table

</td>
<td>

h1|h2|h3
--|--|--
a1|a2|a3
b1|b2|b3
    
</td>
</tr>
<tr>
<td colspan=2>

#### wide cell

</td>
</tr>
<tr>
<td rowspan=2>

#### first cell long

</td>
<td>

cell next to vspanned cell on same row

</td>
</tr>
<tr>
<td>

cell next to vspanned cell on next row

</td>
</tr>
</table>